from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from django.shortcuts import render


urlpatterns = [
    # Django admin
    path('admin/', admin.site.urls),
    # Django front-end
    path('', include('dashboard.urls')),
    path('reports/', include('reports.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('persons/', include('persons.urls')),
    path('patients/', include('patients.urls')),
    path('hospitals/', include('hospitals.urls')),
    path('travels/', include('travels.urls')),
    # API
    path('api/v1/', include('api.rest.v1.urls')),
    path('graphql/', csrf_exempt(GraphQLView.as_view(graphiql=True))),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
