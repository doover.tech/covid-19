# Python imports
import os

# fetch the common settings
from .common import *
from .extra import *

# ##### APPLICATION CONFIGURATION #########################

# allow all hosts during development
ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'dal',
    'dal_select2',
]
INSTALLED_APPS += DEFAULT_APPS
INSTALLED_APPS += [
    'django.contrib.gis',
    'corsheaders',
    'rest_framework',
    'django_filters',
    'drf_yasg',
    #apps
    'django_extensions',
    'graphene_django',
    'users.apps.UsersConfig',
    'locations.apps.LocationsConfig',
    'persons.apps.PersonsConfig',
    'hospitals.apps.HospitalsConfig',
    'patients.apps.PatientsConfig',
    'travels.apps.TravelsConfig',
    'jobs.apps.JobsConfig',
    'dashboard.apps.DashboardConfig',
    'reports.apps.ReportsConfig'
]


# ##### DATABASE CONFIGURATION ############################
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(PROJECT_ROOT / 'run' / 'dev.sqlite3'),
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'gis',
        'USER': 'username',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '5434'
    },
}

# ##### DEBUG CONFIGURATION ###############################
DEBUG = True
