import graphene
import graphql_jwt

# Register your schemes here.

import api.graphql.users.schema as users
import api.graphql.locations.schema as locations
import api.graphql.hospitals.schema as hospitals
import api.graphql.persons.schema as persons
import api.graphql.jobs.schema as jobs
import api.graphql.travels.schema as travels
import api.graphql.patients.schema as patients


class Query(
    users.Query,
    patients.Query,
    graphene.ObjectType
):
    pass

class Mutation(
    graphene.ObjectType
):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
