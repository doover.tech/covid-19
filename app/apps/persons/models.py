from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

# Create your models here.

from utils.models import BaseModel, DateModel
from locations.models import Country, Address
from jobs.models import Job

from utils.image import upload_photo


class Person(DateModel):
    class GenderChoices(models.IntegerChoices):
        FEMALE = 0, _('persons.gender_choices.female')
        MALE = 1, _('persons.gender_choices.male')

    iin = models.CharField(
        max_length=12,
        unique=True,
        blank=True,
        null=True,
        verbose_name=_('persons.person.iin')
    )
    passport = models.CharField(
        max_length=32,
        unique=True,
        blank=True,
        null=True,
        verbose_name=_('persons.person.passport')
    )
    first_name = models.CharField(
        max_length=256,
        verbose_name=_('persons.person.first_name')
    )
    last_name = models.CharField(
        max_length=256,
        verbose_name=_('persons.person.last_name')
    )
    middle_name = models.CharField(
        max_length=256,
        blank=True,
        verbose_name=_('persons.person.middle_name')
    )
    gender = models.IntegerField(
        choices=GenderChoices.choices,
        default=0,
        verbose_name=_('persons.person.gender')
    )
    birth_date = models.DateField(
        null=True,
        verbose_name=_('persons.person.birth_date')
    )
    image = models.ImageField(
        default='persons/profile/default.png',
        upload_to=upload_photo,
        verbose_name=_('persons.person.image'),
        blank=True
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('persons.person.verbose')
        verbose_name_plural = _('persons.person.plural')
    
    def __str__(self) -> str:
        return str(self.iin) if self.iin else str(self.passport)

    @property
    def full_name(self) -> str:
        return f'{self.last_name} {self.first_name} {self.middle_name}'.strip()

    @property
    def age(self):
        if not self.birth_date:
            return 0
        result = timezone.localdate() - self.birth_date
        return int(result.days / 365.2425)

    def get_absolute_url(self):
        return reverse('person_detail', kwargs={'pk': self.pk})


class PersonContact(DateModel):
    class ContactChoices(models.IntegerChoices):
        phone = 0, _('persons.contact_choices.phone')
        email = 1, _('persons.contact_choices.email')

    person = models.ForeignKey(
        to=Person,
        on_delete=models.CASCADE,
        related_name='contacts',
        verbose_name=_('persons.person_contact.person')
    )
    kind = models.IntegerField(
        choices=ContactChoices.choices,
        verbose_name=_('persons.person_contact.kind')
    )
    value = models.CharField(
        max_length=64,
        verbose_name=_('persons.person_contact.value')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('persons.person_contact.verbose')
        verbose_name_plural = _('persons.person_contact.plural')
        constraints = [
            models.UniqueConstraint(fields=['person', 'value'], name='person_contact_unique') 
        ]

    def __str__(self) -> str:
        return str(self.person)


class PersonExtra(DateModel):
    person = models.OneToOneField(
        to=Person,
        on_delete=models.CASCADE,
        related_name='extras',
        verbose_name=_('persons.person_extra.person')
    )
    address = models.ForeignKey(
        to=Address,
        on_delete=models.SET_NULL,
        related_name='persons',
        blank=True,
        null=True,
        verbose_name=_('persons.person_extra.address')
    )
    job = models.ForeignKey(
        Job,
        on_delete=models.SET_NULL,
        related_name='persons',
        null=True,
        blank=True,
        verbose_name=_('persons.person_extra.job')
    )
    citizenship = models.ForeignKey(
        to=Country,
        on_delete=models.SET_NULL,
        related_name='persons',
        blank=True,
        null=True,
        verbose_name=_('persons.person_extra.citizenship')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('persons.person_extra.verbose')
        verbose_name_plural = _('persons.person_extra.plural')

    def __str__(self) -> str:
        return str(self.person)
