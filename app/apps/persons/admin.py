from django.contrib import admin

# Register your models here.

from .models import Person, PersonExtra, PersonContact


class PersonExtraAdmin(admin.StackedInline):
    model = PersonExtra
    autocomplete_fields = ('address', 'job', 'citizenship', )


class PersonContactAdmin(admin.StackedInline):
    model = PersonContact
    extra = 1


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('iin', 'passport', 'first_name', 'last_name', 'middle_name', )
    list_filter = ('gender', )
    search_fields = ('iin', 'passport', )
    fieldsets = [
        (
            ' ', {
                'fields': ('iin', 'passport', 'image', )
            }
        ), (
            ' ', {
                'fields': ('first_name', 'last_name', 'middle_name', )
            }
        ), (
            ' ', {
                'fields': ('gender', 'birth_date', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )
    inlines = [
        PersonExtraAdmin,
        PersonContactAdmin,
    ]