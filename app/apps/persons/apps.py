from django.apps import AppConfig

# Register your config here.

from django.utils.translation import ugettext_lazy as _


class PersonsConfig(AppConfig):
    name = 'persons'
    verbose_name = _('persons.app')
