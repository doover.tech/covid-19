from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _

# Create your managers here.


class CustomUserManager(BaseUserManager):
    def create_user(self, iin, password, **extra_fields):
        if not iin:
            raise ValueError(_('users.custom_user_manager.value_error.not_iin'))
        user = self.model(iin=iin, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, iin, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('users.custom_user_manager.value_error.not_staff'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('users.custom_user_manager.value_error.not_superuser'))
        return self.create_user(iin, password, **extra_fields)
