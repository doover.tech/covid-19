from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

# Create your models here.

from .managers import CustomUserManager
from utils.models import DateModel
from persons.models import Person
from locations.models import Region


class CustomUser(DateModel, AbstractBaseUser, PermissionsMixin):
    iin = models.CharField(
        max_length=12,
        unique=True,
        verbose_name=_('users.custom_user.iin')
    )
    is_staff = models.BooleanField(
        default=False,
        verbose_name=_('users.custom_user.is_staff')
    )
    is_active = models.BooleanField(
        default=False,
        verbose_name=_('users.custom_user.is_active')
    )
    person = models.ForeignKey(
        to=Person,
        on_delete=models.CASCADE,
        related_name='users',
        verbose_name=_('users.custom_user.person'),
        blank=True,
        null=True
    )
    region = models.ForeignKey(
        to=Region,
        on_delete=models.CASCADE,
        related_name='users',
        verbose_name=_('users.custom_user.region'),
        blank=True,
        null=True
    )

    USERNAME_FIELD = 'iin'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('users.custom_user.verbose')
        verbose_name_plural = _('users.custom_user.plural')

    def __str__(self) -> str:
        return self.iin
