from django.apps import AppConfig

# Register your config here.

from django.utils.translation import ugettext_lazy as _


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = _('users.app')
