from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('iin', 'is_staff', 'is_active',)
    list_filter = ('iin', 'is_staff', 'is_active',)
    fieldsets = (
        (
            ' ', {
                'fields': ('iin', 'password', 'person', 'region')
            }
        ), (
            ' ', {
                'fields': ('groups', )
            }
        ), (
            ' ', {
                'fields': ('is_staff', 'is_active', )
            }
        )
    )
    add_fieldsets = (
        (
            ' ', {
            'classes': ('wide',),
            'fields': ('iin', 'password1', 'password2', )
            }
        ), (
            ' ', {
                'fields': ('is_staff', 'is_active', )
            }
        )
    )
    search_fields = ('iin',)
    ordering = ('iin',)
