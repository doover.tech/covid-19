from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

# Create your models here.

from utils.models import DateModel
from locations.models import Address, Region


class HospitalCategory(DateModel):
    name = models.CharField(
        max_length=256,
        verbose_name=_('hospitals.hospital_category.name')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('hospitals.hospital_category.verbose')
        verbose_name_plural = _('hospitals.hospital_category.plural')

    def __str__(self) -> str:
        return str(self.name)


class Hospital(DateModel):
    class IsChoices(models.IntegerChoices):
        no = 0, _('hospitals.is_choices.no')
        yes = 1, _('hospitals.is_choices.yes')

    category = models.ForeignKey(
        to=HospitalCategory,
        on_delete=models.SET_NULL,
        related_name='hospitals',
        blank=True,
        null=True,
        verbose_name=_('hospitals.hospital.category')
    )
    region = models.ForeignKey(
        to=Region,
        on_delete=models.SET_NULL,
        related_name='hospitals',
        blank=True,
        null=True,
        verbose_name=_('hospitals.hospital.region')
    )
    address = models.ForeignKey(
        to=Address,
        on_delete=models.SET_NULL,
        related_name='hospitals',
        blank=True,
        null=True,
        verbose_name=_('hospitals.hospital.address')
    )
    name = models.CharField(
        max_length=256,
        verbose_name=_('hospitals.hospital.name')
    )
    is_heal = models.BooleanField(
        choices=IsChoices.choices,
        default=False,
        verbose_name=_('hospitals.hospital.is_heal')
    )
    is_diagnostic = models.BooleanField(
        choices=IsChoices.choices,
        default=False,
        verbose_name=_('hospitals.hospital.is_diagnostic')
    )
    beds_amount = models.PositiveIntegerField(
        default=0,
        verbose_name=_('hospitals.hospital.beds_amount')
    )
    staff_amount = models.PositiveIntegerField(
        default=0,
        verbose_name=_('hospitals.hospital.staff_amount')
    )
    tests_amount = models.PositiveIntegerField(
        default=0,
        verbose_name=_('hospitals.hospital.tests_amount')
    )
    tests_used = models.PositiveIntegerField(
        default=0,
        verbose_name=_('hospitals.hospital.tests_used')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('hospitals.hospital.verbose')
        verbose_name_plural = _('hospitals.hospital.plural')
        permissions = [
            ('self_region', "Editing your region"),
        ]

    def __str__(self) -> str:
        return str(self.name)

    def get_absolute_url(self):
        return reverse('hospitals:hospital.detail', kwargs={'pk': self.pk})
