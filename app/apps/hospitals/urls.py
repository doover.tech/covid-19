from django.urls import path

from .views import HospitalList, HospitalDetail

app_name = 'hospitals'

urlpatterns = [
    path('', HospitalList.as_view(), name='hospital.list'),
    path('<uuid:pk>/', HospitalDetail.as_view(), name='hospital.detail'),
]
