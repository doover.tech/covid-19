from django.contrib import admin

# Register your models here.

from .models import Hospital, HospitalCategory


@admin.register(HospitalCategory)
class HospitalCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(Hospital)
class HospitalAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'region', 'is_heal', 'is_diagnostic', 'tests_amount', 'tests_used', )
    list_filter = ('category', 'is_heal', 'is_diagnostic', 'region', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', 'is_heal', 'is_diagnostic', )
            }
        ), (
            ' ', {
                'fields': ('category', 'region', 'address', )
            }
        ), (
            ' ', {
                'fields': ('beds_amount', 'staff_amount', 'tests_amount', 'tests_used')
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )
