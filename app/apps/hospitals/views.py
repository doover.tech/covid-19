from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

from .models import Hospital, HospitalCategory


class HospitalList(LoginRequiredMixin, ListView):
    model = Hospital


class HospitalDetail(LoginRequiredMixin, DetailView):
    model = Hospital
