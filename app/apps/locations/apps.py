from django.apps import AppConfig

# Register your config here.

from django.utils.translation import ugettext_lazy as _


class LocationsConfig(AppConfig):
    name = 'locations'
    verbose_name = _('locations.app')
