from django.db import models
from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import Point

# Create your models here.

from utils.models import DateModel
from utils.yandex import get_address


class Country(DateModel):
    name = models.CharField(
        max_length=256,
        verbose_name=_('locations.country.name')
    )
    iso = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=_('locations.country.iso')
    )
    mok = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=_('locations.country.mok')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('locations.country.verbose')
        verbose_name_plural = _('locations.country.plural')

    def __str__(self) -> str:
        return self.name


class Region(DateModel):
    country = models.ForeignKey(
        to=Country,
        on_delete=models.CASCADE,
        related_name='regions',
        verbose_name=_('locations.region.country')
    )
    name = models.CharField(
        max_length=256,
        verbose_name=_('locations.region.name')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('locations.region.verbose')
        verbose_name_plural = _('locations.region.plural')

    def __str__(self) -> str:
        return self.name


class City(DateModel):
    country = models.ForeignKey(
        to=Country,
        on_delete=models.CASCADE,
        related_name='cities',
        verbose_name=_('locations.city.country')
    )
    region = models.ForeignKey(
        to=Region,
        on_delete=models.CASCADE,
        related_name='cities',
        blank=True,
        null=True,
        verbose_name=_('locations.city.region')
    )
    name = models.CharField(
        max_length=256,
        verbose_name=_('locations.city.name')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('locations.city.verbose')
        verbose_name_plural = _('locations.city.plural')

    def __str__(self) -> str:
        return self.name


class AddressCategory(DateModel):
    name = models.CharField(
        max_length=256,
        verbose_name=_('locations.address_category.name')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('locations.address_category.verbose')
        verbose_name_plural = _('locations.address_category.plural')

    def __str__(self) -> str:
        return self.name


class Address(DateModel):
    city = models.ForeignKey(
        to=City,
        on_delete=models.SET_NULL,
        related_name='addresses',
        blank=True,
        null=True,
        verbose_name=_('locations.address.city')
    )
    name = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=_('locations.address.name')
    )
    street = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=_('locations.address.street')
    )
    house = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=_('locations.address.house')
    )
    flat = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=_('locations.address.flat')
    )
    building = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=_('locations.address.building')
    )
    point = models.PointField(
        blank=True,
        null=True,
        verbose_name=_('locations.address.point')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('locations.address.verbose')
        verbose_name_plural = _('locations.address.plural')

    def __str__(self) -> str:
        if self.city:
            return f'{self.city} / {self.name}'
        return self.name

    def save(self, *args, **kwargs):
        address = f'{self.city} {self.street} {self.house}{self.building}'
        try:
            yandex_data = get_address(address)
            if (addresses_list := yandex_data.get('response', []).get('GeoObjectCollection', []).get('featureMember')):
                geo_object = addresses_list[0]['GeoObject']
                point = geo_object['Point']['pos'].split(' ')[::-1]
                self.name = geo_object['name']
                self.street = geo_object['metaDataProperty']['GeocoderMetaData']['Address']['Components'][3]['name']
                self.house = geo_object['metaDataProperty']['GeocoderMetaData']['Address']['Components'][4]['name']
                self.point = Point(*map(float, point))
        except Exception as e:
            pass
        super().save(*args, **kwargs)
