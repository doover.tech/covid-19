from django.contrib import admin
from django.contrib.gis import admin

# Register your models here.

from .models import Country, City, Region, Address, AddressCategory


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', 'iso', 'mok', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'fields': ('iso', 'mok', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', )
    list_filter = ('country', )
    autocomplete_fields = ('country', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'fields': ('country', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'region')
    list_filter = ('country', 'region', )
    autocomplete_fields = ('country', 'region', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'fields': ('country', 'region')
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(AddressCategory)
class AddressCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(Address)
class AddressAdmin(admin.OSMGeoAdmin):
    list_display = ('city', 'name', 'street', 'house', 'flat', 'building', 'point', )
    list_filter = ('city', )
    autocomplete_fields = ('city', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', 'city', )
            }
        ), (
            ' ', {
                'fields': ('street', 'house', 'flat', 'building', )
            }
        ), (
            ' ', {
                'fields': ('point', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )
    default_zoom = 1
