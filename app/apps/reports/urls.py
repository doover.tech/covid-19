from django.urls import path

from .views import PatientReport

app_name = 'reports'

urlpatterns = [
    path('patients/', PatientReport.as_view())
]
