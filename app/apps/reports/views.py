from django.shortcuts import render
from django.views import View
from .forms import UploadFileForm
from .utils import handle_uploaded_patients

class PatientReport(View):
    def render(self, request):
        form = UploadFileForm()
        return render(request, 'reports/patients.html', context={'form': form})

    def get_form(self):
        return UploadFileForm

    def get(self, request):
        return self.render(request)

    def post(self, request):
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_patients(request.FILES['file'])
            print('SUCCESS')
        return self.render(request)