from django.apps import AppConfig

# Register your config here.

from django.utils.translation import ugettext_lazy as _


class TravelsConfig(AppConfig):
    name = 'travels'
    verbose_name = _('travels.app')
