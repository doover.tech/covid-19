from django.urls import path

from .views import TravelList, TravelDetail

app_name = 'travels'

urlpatterns = [
    path('', TravelList.as_view(), name='travel.list'),
    path('<uuid:pk>/', TravelDetail.as_view(), name='travel.detail'),
]
