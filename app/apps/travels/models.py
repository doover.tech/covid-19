from django.db import models
from django.urls import reverse
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _

# Create your models here.

from utils.models import BaseModel, DateModel
from locations.models import Country
from patients.models import Patient


class Category(DateModel):
    name = models.CharField(
        max_length=256,
        verbose_name=_('travels.category.name')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('travels.category.verbose')
        verbose_name_plural = _('travels.category.plural')

    def __str__(self) -> str:
        return str(self.name)


class RoadMap(DateModel):
    country_from = models.ForeignKey(
        to=Country,
        on_delete=models.PROTECT,
        related_name='maps_from',
        verbose_name=_('travels.road_map.country_from')
    )
    country_to = models.ForeignKey(
        to=Country,
        on_delete=models.PROTECT,
        related_name='maps_to',
        verbose_name=_('travels.road_map.country_to')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('travels.road_map.verbose')
        verbose_name_plural = _('travels.road_map.plural')

    def __str__(self):
        return f'{self.country_from} - {self.country_to}'


class Travel(DateModel):
    arrival = models.ForeignKey(
        to=RoadMap,
        on_delete=models.PROTECT,
        related_name='travels',
        verbose_name=_('travels.flight.arrival')
    )
    patients = models.ManyToManyField(
        to=Patient,
        related_name='travels',
        through='Passenger',
        blank=True,
        verbose_name=_('travels.flight.patients')
    )
    category = models.ForeignKey(
        to=Category,
        on_delete=models.CASCADE,
        related_name='travels',
        verbose_name=_('travels.flight.category')
    )
    date_at = models.DateField(
        verbose_name=_('travels.flight.date_at')
    )
    name = models.CharField(
        max_length=256,
        verbose_name=_('travels.flight.name')
    )
    data = JSONField(
        default=dict,
        blank=True,
        verbose_name=_('travels.flight.data')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('travels.travel.verbose')
        verbose_name_plural = _('travels.travel.plural')

    def __str__(self) -> str:
        return str(self.arrival)

    def get_absolute_url(self):
        return reverse('travels:travel.detail', kwargs={'pk': self.pk})


class Passenger(DateModel):
    travel = models.ForeignKey(
        to=Travel,
        on_delete=models.CASCADE,
        verbose_name=_('travels.passenger.travel')
    )
    patient = models.ForeignKey(
        to=Patient,
        on_delete=models.CASCADE,
        verbose_name=_('travels.passenger.patient')
    )
    seat = models.CharField(
        max_length=32,
        blank=True,
        verbose_name=_('travels.passenger.seat')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('travels.passenger.verbose')
        verbose_name_plural = _('travels.passenger.plural')
