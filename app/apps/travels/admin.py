from django.contrib import admin

# Register your models here.

from .models import RoadMap, Travel, Category, Passenger


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(RoadMap)
class RoadMapAdmin(admin.ModelAdmin):
    list_display = ('country_from', 'country_to', )
    autocomplete_fields = ('country_from', 'country_to', )
    search_fields = ('country_from__name', 'country_to__name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('country_from', 'country_to', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(Travel)
class TravelAdmin(admin.ModelAdmin):
    list_display = ('arrival', 'date_at', 'name', )
    autocomplete_fields = ('arrival', 'category', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('arrival', 'category', )
            }
        ), (
            ' ', {
                'fields': ('name', 'date_at', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(Passenger)
class PassengerAdmin(admin.ModelAdmin):
    list_display = ('travel', 'patient', )
    autocomplete_fields = ('travel', 'patient', )
    search_fields = ('seat', )
    fieldsets = [
        (
            ' ', {
                'fields': ('travel', 'patient', )
            }
        ), (
            ' ', {
                'fields': ('seat', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )
