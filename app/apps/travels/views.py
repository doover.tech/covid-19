from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

from .models import RoadMap, Travel


class TravelList(LoginRequiredMixin, ListView):
    model = Travel
    paginate_by = 50
    ordering = ['created_at']

    def get_ordering(self):
        ordering_list = ['created_at', 'updated_at', ]
        ordering = self.request.GET.get('ordering', 'created_at')
        if ordering in ordering_list:
            return ordering
        return 'created_at'


class TravelDetail(LoginRequiredMixin, DetailView):
    model = Travel
