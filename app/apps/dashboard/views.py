from django.shortcuts import render
from django.views import View
from django.db.models import Count, Case, When
from patients.models import Patient
from locations.models import Region


def index(request):
    
    queryset = Patient.objects.all()

    context = {
        'dashboard': {
            'total': queryset.count(),
            'alive': queryset.filter(status__type_status=1).count(),
            'dead': queryset.filter(status__type_status=0).count()
        }
    }
    return render(request, 'index.html', context=context)


class IndexView(View):
    def get(self, request):
        queryset = Patient.objects.all()
        regions = Region.objects.annotate(count=Count('cities__addresses__persons__person__patient')).order_by('-count')

        context = {
            'dashboard': {
                'total': queryset.count(),
                'alive': queryset.filter(status__type_status=1).count(),
                'dead': queryset.filter(status__type_status=0).count()
            },
            'regions': regions
        }
        print([region.count for region in regions])
        return render(request, 'index.html', context=context)
