from django.contrib import admin

# Register your models here.

from .models import Patient, PatientStatus, State, PatientState


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


class PatientStateAdmin(admin.StackedInline):
    model = PatientState
    extra = 1
    autocomplete_fields = ('state', )


class PatientStatusAdmin(admin.StackedInline):
    model = PatientStatus


@admin.register(Patient)
class PatientAdmin(admin.ModelAdmin):
    list_display = ('person', )
    filter_horizontal = ('contacted', )
    autocomplete_fields = ('person', 'hospital', 'create_by', 'region', )
    search_fields = ('person__iin', )
    fieldsets = [
        (
            ' ', {
                'fields': ('person', 'hospital', 'region', 'create_by', )
            }
        ), (
            ' ', {
                'fields': ('contacted', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )
    inlines = [
        PatientStatusAdmin,
        PatientStateAdmin,
    ]
