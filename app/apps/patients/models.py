from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

# Create your models here.

from utils.models import BaseModel, DateModel
from persons.models import Person
from hospitals.models import Hospital
from locations.models import Region


class Patient(DateModel):
    person = models.OneToOneField(
        to=Person,
        on_delete=models.PROTECT,
        related_name='patient',
        verbose_name=_('patients.patient.person'),
        null=True
    )
    hospital = models.ForeignKey(
        to=Hospital,
        on_delete=models.SET_NULL,
        related_name='patients',
        blank=True,
        null=True,
        verbose_name=_('patients.patient.hospital')
    )
    region = models.ForeignKey(
        to=Region,
        on_delete=models.SET_NULL,
        related_name='patients',
        blank=True,
        null=True,
        verbose_name=_('patients.patient.region')
    )
    contacted = models.ManyToManyField(
        to='self',
        blank=True,
        verbose_name=_('patients.patient.contacted')  
    )
    create_by = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.SET_NULL,
        related_name='patients',
        blank=True,
        null=True,
        verbose_name=_('patients.patient.create_by')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('patients.patient.verbose')
        verbose_name_plural = _('patients.patient.plural')
        permissions = [
            ('self_region', "Editing your region"),
            ('self_patient', "Editing your patient"),
        ]

    def __str__(self) -> str:
        return f'{self.person.iin} - {self.person.full_name}'

    def get_absolute_url(self):
        return reverse('patients:patient.detail', kwargs={'pk': self.pk})


class PatientStatus(DateModel):
    class MethodInfectionChoices(models.IntegerChoices):
        sample = 0, _('patients.method_infection_choices.sample')

    class CaseChoices(models.IntegerChoices):
        local = 0, _('patients.case_choices.local')
        imported = 1, _('patients.case_choices.imported')
        contact = 2, _('patients.case_choices.contact')

    class EntryChoices(models.IntegerChoices):
        sample = 0, _('patients.entry_choices.sample')

    class InfectionChoices(models.IntegerChoices):
        infected = 1, _('patients.infection_choices.infected')
        contact = 2, _('patients.infection_choices.contact')
        potential = 3, _('patients.infection_choices.potential')

    class HospitalizationChoices(models.IntegerChoices):
        home = 0, _('patients.hospitalization_choices.home')
        hospitalized = 1, _('patients.hospitalization_choices.hospitalized')

    class StatusChoices(models.IntegerChoices):
        dead = 0, _('patients.status_choices.dead')
        alive = 1, _('patients.status_choices.alive')

    patient = models.OneToOneField(
        to=Patient,
        on_delete=models.CASCADE,
        related_name='status',
        verbose_name=_('patients.patient_status.patient')
    )
    method_infection = models.IntegerField(
        choices=MethodInfectionChoices.choices,
        blank=True,
        null=True,
        verbose_name=_('patients.patient_status.method_infection')
    )
    type_case = models.IntegerField(
        choices=CaseChoices.choices,
        blank=True,
        null=True,
        verbose_name=_('patients.patient_status.type_case')
    )
    type_entry = models.IntegerField(
        choices=EntryChoices.choices,
        blank=True,
        null=True,
        verbose_name=_('patients.patient_status.type_entry')
    )
    type_infection = models.IntegerField(
        choices=InfectionChoices.choices,
        blank=True,
        null=True,
        verbose_name=_('patients.patient_status.type_infection')
    )
    type_hospitalization = models.IntegerField(
        choices=HospitalizationChoices.choices,
        blank=True,
        null=True,
        verbose_name=_('patients.patient_status.type_hospitalization')
    )
    type_status = models.IntegerField(
        choices=StatusChoices.choices,
        blank=True,
        null=True,
        verbose_name=_('patients.patient_status.type_status')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('patients.patient_status.verbose')
        verbose_name_plural = _('patients.patient_status.plural')

    def __str__(self) -> str:
        return str(self.patient)


class State(DateModel):
    name = models.CharField(
        max_length=256,
        verbose_name=_('patients.state.name')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('patients.state.verbose')
        verbose_name_plural = _('patients.state.plural')

    def __str__(self) -> str:
        return str(self.name)


class PatientState(DateModel):
    patient = models.ForeignKey(
        to=Patient,
        on_delete=models.CASCADE,
        related_name='states',
        verbose_name=_('patients.patient_state.patient')
    )
    state = models.ForeignKey(
        to=State,
        on_delete=models.PROTECT,
        related_name='patients',
        null=True,
        verbose_name=_('patients.patient_state.state')
    )
    comment = models.TextField(
        blank=True,
        verbose_name=_('patients.patient_state.comment')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('patients.patient_state.verbose')
        verbose_name_plural = _('patients.patient_state.plural')

    def __str__(self) -> str:
        return str(self.state)
