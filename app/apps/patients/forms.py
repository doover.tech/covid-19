from django import forms
from dal import autocomplete

from utils.forms import ModelBootstrapForm
from .models import Patient, PatientStatus
from persons.models import Person
from locations.models import Address
from jobs.models import Job


class PersonForm(ModelBootstrapForm):
    class Meta:
        model = Person
        exclude = ('image', )

    @classmethod
    def get_instance(self, pk):
        patient = Patient.objects.get(uuid=pk)
        return patient.person


class PatientForm(ModelBootstrapForm):
    class Meta:
        model = Patient
        exclude = ('person', )
        widgets = {
            'contacted': autocomplete.ModelSelect2Multiple(
                'patients:patient.autocomplete'
            )
        }

    def save(self, commit=True):
        instance = forms.ModelForm.save(self, False)
        old_save_m2m = self.save_m2m

        def save_m2m():
            old_save_m2m()
            instance.contacted.clear()
            for contact in self.cleaned_data['contacted']:
                if instance == contact:
                    continue
                instance.contacted.add(contact)

        instance.save()
        self.save_m2m()

        return instance

    @classmethod
    def get_instance(self, pk):
        patient = Patient.objects.get(uuid=pk)
        return patient


class PatientStatusForm(ModelBootstrapForm):
    class Meta:
        model = PatientStatus
        exclude = ('patient', )

    @classmethod
    def get_instance(self, pk):
        patient = Patient.objects.get(uuid=pk)
        return patient.status


class PatientAddressForm(ModelBootstrapForm):
    class Meta:
        model = Address
        exclude = ('name', 'point', )

    @classmethod
    def get_instance(self, pk):
        patient = Patient.objects.get(uuid=pk)
        return patient.person.extras.address


class JobAddressForm(ModelBootstrapForm):
    class Meta:
        model = Address
        exclude = ('name', 'point', )

    @classmethod
    def get_instance(self, pk):
        patient = Patient.objects.get(uuid=pk)
        return patient.person.extras.job.address


class JobForm(ModelBootstrapForm):
    class Meta:
        model = Job
        exclude = ('address', )

    @classmethod
    def get_instance(self, pk):
        patient = Patient.objects.get(uuid=pk)
        return patient.person.extras.job
