from django.apps import AppConfig

# Register your config here.

from django.utils.translation import ugettext_lazy as _


class PatientsConfig(AppConfig):
    name = 'patients'
    verbose_name = _('patients.app')
