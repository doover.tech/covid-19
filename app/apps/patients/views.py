from django.shortcuts import render
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q

from dal import autocomplete

# Create your views here.

from .models import Patient, PatientStatus, State, PatientState
from .forms import PersonForm, PatientForm, PatientStatusForm, PatientAddressForm, JobAddressForm, JobForm
from persons.models import PersonExtra
from .filters import PatientFilter


class PatientView(LoginRequiredMixin, View):
    def get(self, request):
        f = PatientFilter(request.GET, queryset=Patient.objects.all())
        return render(request, 'patients/patient_filter.html', {'filter': f})


class PatientCreate(LoginRequiredMixin, View):
    def get_forms(self, request, pk=None):
        form_list = [
            PersonForm,
            PatientForm,
            PatientStatusForm,
            PatientAddressForm,
            JobAddressForm,
            JobForm,
        ]

        if request.method == 'POST':
            return [
                item(request.POST, instance=item.get_instance(pk), prefix=index + 1) if pk else item(request.POST, prefix=index + 1)
                for index, item in enumerate(form_list)
            ]
        else:
            return [
                item(instance=item.get_instance(pk), prefix=index + 1) if pk else item(prefix=index + 1)
                for index, item in enumerate(form_list)
            ]

    def save(self, forms):
        person = forms[0].save(commit=False)

        patient = forms[1].save(commit=False)
        patient.person = person

        status = forms[2].save(commit=False)
        status.patient = patient

        person_address = forms[3].save(commit=False)
        job_address = forms[4].save(commit=False)
        job = forms[5].save(commit=False)
        job.address = job_address

        person.save()
        person_address.save()
        patient.save()
        status.save()
        job_address.save()
        job.save()

        person_extra, _ = PersonExtra.objects.get_or_create(
            person=person,
            defaults={
                'address': person_address,
                'job': job
            }
        )

        return redirect('patients:patient.list')

    def get(self, request, pk=None):
        forms = self.get_forms(request, pk)
        context = {
            'forms': forms,
            'type': 'update' if pk else 'create'
        }
        return render(request, 'patients/patient_form.html', context=context)

    def post(self, request, pk=None):
        forms = self.get_forms(request, pk)
        context = {
            'forms': forms,
            'type': 'update' if pk else 'create'
        }

        if all([form.is_valid() for form in forms]):
            return self.save(forms)

        return render(request, 'patients/patient_form.html', context=context)


class PatientDelete(LoginRequiredMixin, DeleteView):
    model = Patient
    success_url = reverse_lazy('patients:patient.list')


class PatientDetail(LoginRequiredMixin, DetailView):
    model = Patient


class PatientAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Patient.objects.none()

        qs = Patient.objects.all()

        if self.q:
            qs = qs.filter(
                Q(person__iin__icontains=self.q) |
                Q(person__last_name__icontains=self.q) |
                Q(person__first_name__icontains=self.q) |
                Q(person__middle_name__icontains=self.q)
            )

        return qs