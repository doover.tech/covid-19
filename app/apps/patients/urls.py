from django.urls import path

from django_filters.views import FilterView
from .models import Patient
from .views import PatientView, PatientDetail, PatientCreate, PatientDelete
from .views import PatientAutocomplete

app_name = 'patients'

urlpatterns = [
    path('', PatientView.as_view(), name='patient.list'),
    path('list/', FilterView.as_view(model=Patient), name='patient.filter.list'),
    path('create/', PatientCreate.as_view(), name='patient.create'),
    path('<uuid:pk>/', PatientDetail.as_view(), name='patient.detail'),
    path('<uuid:pk>/update/', PatientCreate.as_view(), name='patient.update'),
    path('<uuid:pk>/delete/', PatientDelete.as_view(), name='patient.delete'),
    path('patient-autocomplete/', PatientAutocomplete.as_view(), name='patient.autocomplete'),
]
