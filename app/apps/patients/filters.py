import django_filters
from django.utils.translation import ugettext_lazy as _

from utils.filters import BaseFilterSet
from .models import Patient


class PatientFilter(BaseFilterSet):
    o = django_filters.OrderingFilter(
        fields = (
            ('created_at', 'created_at'),
            ('person__birth_date', 'person__birth_date'),
            ('person__iin', 'person__iin'),
        ),
        field_labels = {
            'created_at': 'по дате добавления',
            'person__birth_date': 'по дате рождения',
            'person__iin': 'по ИИН',
        }
    )

    class Meta:
        model = Patient
        fields = {
            'person__last_name': ['icontains'],
            'person__first_name': ['icontains'],
            'person__middle_name': ['icontains'],
            'person__iin': ['exact'],
            'person__passport': ['exact'],
            'person__extras__job__category': ['exact'],
            'status__method_infection': ['exact'],
            'status__type_case': ['exact'],
            'status__type_entry': ['exact'],
            'status__type_infection': ['exact'],
            'status__type_hospitalization': ['exact'],
            'status__type_status': ['exact'],
            'person__extras__address__city': ['exact'],
            'person__extras__address__city__region': ['exact'],
            'travels__category': ['exact'],
        }
        labels = {
            'person__last_name__icontains': _('persons.person.last_name'),
            'person__first_name__icontains': _('persons.person.first_name'),
            'person__middle_name__icontains': _('persons.person.middle_name'),
            'person__iin': _('persons.person.iin'),
            'person__passport': _('persons.person.passport'),
            'person__extras__job__category': _('jobs.job.category'),
            'status__method_infection': _('patients.patient_status.method_infection'),
            'status__type_case': _('patients.patient_status.type_case'),
            'status__type_entry': _('patients.patient_status.type_entry'),
            'status__type_infection': _('patients.patient_status.type_infection'),
            'status__type_hospitalization': _('patients.patient_status.type_hospitalization'),
            'status__type_status': _('patients.patient_status.type_status'),
            'person__extras__address__city': _('locations.address.city'),
            'person__extras__address__city__region': _('locations.city.region'),
            'travels__category': _('travels.flight.category'),
        }
