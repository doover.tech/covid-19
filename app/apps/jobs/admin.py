from django.contrib import admin

# Register your models here.

from .models import Job, JobCategory, JobEmployment


@admin.register(JobEmployment)
class JobEmploymentAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(JobCategory)
class JobCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'category', 'position', )
    list_filter = ('address', 'category', 'employment', )
    autocomplete_fields = ('address', 'category', 'employment', )
    search_fields = ('name', )
    fieldsets = [
        (
            ' ', {
                'fields': ('name', 'position', )
            }
        ), (
            ' ', {
                'fields': ('address', 'category', 'employment', )
            }
        ), (
            ' ', {
                'classes': ('collapse', ),
                'fields': ('uuid', 'created_at', 'updated_at', )
            }
        )
    ]
    readonly_fields = ('uuid', 'created_at', 'updated_at', )
