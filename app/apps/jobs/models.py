from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

from utils.models import DateModel
from locations.models import Address


class JobCategory(DateModel):
    name = models.CharField(
        max_length=256,
        verbose_name=_('jobs.job_category.name')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('jobs.job_category.verbose')
        verbose_name_plural = _('jobs.job_category.plural')

    def __str__(self) -> str:
        return str(self.name)


class JobEmployment(DateModel):
    name = models.CharField(
        max_length=256,
        verbose_name=_('jobs.job_employment.name')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('jobs.job_employment.verbose')
        verbose_name_plural = _('jobs.job_employment.plural')

    def __str__(self) -> str:
        return str(self.name)


class Job(DateModel):
    address = models.ForeignKey(
        to=Address,
        on_delete=models.SET_NULL,
        related_name='jobs',
        blank=True,
        null=True,
        verbose_name=_('jobs.job.address')
    )
    category = models.ForeignKey(
        to=JobCategory,
        on_delete=models.SET_NULL,
        related_name='jobs',
        blank=True,
        null=True,
        verbose_name=_('jobs.job.category')
    )
    employment = models.ForeignKey(
        to=JobEmployment,
        on_delete=models.SET_NULL,
        related_name='jobs',
        blank=True,
        null=True,
        verbose_name=_('jobs.job.employment')
    )
    name = models.CharField(
        max_length=256,
        verbose_name=_('jobs.job.name')
    )
    position = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=_('jobs.job.position')
    )

    class Meta:
        ordering = ('created_at', )
        verbose_name = _('jobs.job.verbose')
        verbose_name_plural = _('jobs.job.plural')

    def __str__(self) -> str:
        return str(self.name)
