from uuid import uuid4
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class BaseModel(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid4,
        verbose_name=_('utils.base_model.uuid')
    )

    class Meta:
        abstract = True
        ordering = ('uuid', )


class DateModel(BaseModel):
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('utils.date_model.created_at')
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=_('utils.date_model.updated_at')
    )

    class Meta:
        abstract = True
        ordering = ('created_at', 'updated_at')
