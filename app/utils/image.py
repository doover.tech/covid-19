import uuid


def get_image_path(instance, filename, callback=None):
    ext = filename.split('.')[-1]
    filename = f'{uuid.uuid4()}.{ext}'
    folder_path = f'upload_to/'

    if callback:
        return filename

    return folder_path + filename


def upload_photo(instance, filename):
    filename = get_image_path(instance, filename, True)
    folder_path = f'persons/profile/'

    return folder_path + filename
