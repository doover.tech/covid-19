import django_filters


class BaseFilterSet(django_filters.FilterSet):
    class Meta:
        abstract = True
        fields = {}
        labels = {}

    def __init__(self, *args, **kwargs):
        super(BaseFilterSet, self).__init__(*args, **kwargs)
        for item in self.filters:
            if item in self.Meta.labels.keys():
                self.filters[item].label = self.Meta.labels[item].title()
            self.filters[item].field.widget.attrs.update({'class': 'form-control'})
