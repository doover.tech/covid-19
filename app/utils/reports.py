import csv
from io import StringIO
from persons.models import Person, PersonContact, PersonExtra
from jobs.models import Job
from locations.models import Address, City, Country, Region
from patients.models import Patient, PatientStatus
from hospitals.models import Hospital
from utils.yandex import get_address
import datetime


def get_or_create_address(address_data):
    address = f'{address_data["city"]} {address_data["street"]} {address_data["house"]}{address_data["building"]}'
    yandex_data = get_address(address)

    try:
        if (addresses_list := yandex_data.get('response', []).get('GeoObjectCollection', []).get('featureMember')):
            geo_object = addresses_list[0]['GeoObject']
            address = Address.objects.get(name=geo_object['name'])
        else:
            raise TypeError()
    except:
        address = Address.objects.create(
            **address_data
        )
    return address

def string_to_date(date):
    date = date.split('.')
    date = datetime.date(
        year=int(date[-1]),
        month=int(date[1]),
        day=int(date[0])
    )
    return date

def handle_uploaded_patients(f):
    file = f.read().decode('utf-8')
    csv_data = csv.DictReader(StringIO(file), delimiter=',')

    for line in csv_data:
        # Countries
        person_country_name = line.get('Страна')
        job_country_name = line.get('Страна компании')

        # Regions
        person_region_name = line.get('Регион')
        job_region_name = line.get('Регион компании')

        # Cities
        person_city_name = line.get('Город')
        job_city_name = line.get('Город компании')

        # Person data
        gender = {'Муж': 1, 'Жен': 0}

        person_data = {
            'first_name': line.get('Имя'),
            'last_name': line.get('Фамилия'),
            'middle_name': line.get('Отчество'),
            'iin': line.get('ИИН'),
            'passport': line.get('Паспорт'),
            'gender': gender[line.get('Пол')],
            'birth_date': string_to_date(line['Дата рождения']) if line.get('Дата рождения') else None
        }

        # Patient data
        type_infection = {
            'Инфицирован': 1,
            'Контактирован': 2,
            'Потенциальный': 3,
        }
        type_case = {
            'Локальный': 0,
            'Завозной': 1,
            'Контактный': 2,
        }
        type_entry = {
            'Пример': 0
        }
        type_hospitalization = {
            'Дома': 0,
            'Госпитализирован': 1
        }
        type_status = {
            'Мертв': 0,
            'Жив': 1
        }
        method_infection = {
            'Пример': 0
        }

        patient_data = {
            'type_infection': type_infection[line.get('Статус инфецирования')],
            'type_case': type_case[line.get('Тип случая')],
            'type_entry': type_entry[line.get('Тип въезда')],
            'type_hospitalization': type_hospitalization[line.get('Статус госпитализации')],
            'type_status': type_status[line.get('Текущее состяние')],
            'method_infection': method_infection[line.get('Способ заражения')],
        }

        # Country

        person_country, _ = Country.objects.get_or_create(
            name=person_country_name
        )

        job_country, job_country_status = Country.objects.get_or_create(
            name=job_country_name
        )

        # Region

        if person_region_name:
            person_region, _ = Region.objects.get_or_create(
                country=person_country,
                name=person_region_name
            )

        if job_region_name:
            job_region, _ = Region.objects.get_or_create(
                country=job_country,
                name=job_region_name
            )

        # City

        person_city, _ = City.objects.get_or_create(
            name=person_city_name,
            country=person_country,
            defaults={
                'region': person_region
            }
        )

        job_city, _ = City.objects.get_or_create(
            country=job_country,
            name=job_city_name,
            defaults={
                'region': job_region
            }
        )

        # Addresses
        person_address_data = {
            'city': person_city,
            'street': line.get('Адрес'),
            'house': line.get('Дом'),
            'flat': line.get('Квартира'),
            'building': line.get('Здание')

        }

        job_address_data = {
            'city': job_city,
            'street': line.get('Адрес компании'),
            'house': line.get('Дом компании'),
            'flat': line.get('Квартира компании'),
            'building': line.get('Здание компании')
        }

        person_address = get_or_create_address(person_address_data)

        if line.get('Название компании'):
            job_address = get_or_create_address(job_address_data)

            # Job
            print(job_address)
            print(line.get('Название компании'))
            print(job_address_data)
            job, _ = Job.objects.get_or_create(name=line['Название компании'], address=job_address)
        else:
            job = None

        # Person

        person, _ = Person.objects.get_or_create(
            first_name=line.get('Имя'),
            last_name=line.get('Фамилия'),
            middle_name=line.get('Отчество')
        )
        Person.objects.filter(pk=person.pk).update(
            **person_data
        )

        _, _ = PersonContact.objects.get_or_create(
            person=person,
            kind=0,
            value=line['Телефон']
        )

        _, _ = PersonContact.objects.get_or_create(
            person=person,
            kind=1,
            value=line['Эмейл']
        )

        # Person Extra

        person_extra, _ = PersonExtra.objects.get_or_create(
            person=person,
        )

        if job:
            person_extra.job = job
        person_extra.address = person_address
        person_extra.citizenship = person_country
        person_extra.save()

        # Hospital

        hospital, hospital_status = Hospital.objects.get_or_create(name=line['Больница'])

        # Patient

        patient, _ = Patient.objects.get_or_create(
            person=person,
        )
        Patient.objects.filter(person=person).update(
            hospital=hospital,
            region=person_region
        )
        patient_status, _ = PatientStatus.objects.get_or_create(
            patient=patient
        )
        PatientStatus.objects.filter(patient=patient).update(
            **patient_data
        )
