from django import forms


class ModelBootstrapForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModelBootstrapForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            if visible.field.required:
                visible.field.label += ' *'
            visible.field.widget.attrs['class'] = 'form-control'

    class Meta:
        abstract = True
