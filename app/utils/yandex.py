import requests


def get_address(address):
    API_KEY = '29a11bb5-df41-476d-bd95-13585a9260cd'
    response = requests.get(
        url=f'https://geocode-maps.yandex.ru/1.x/?apikey={API_KEY}&geocode={address}&format=json'
    )

    return response.json()
