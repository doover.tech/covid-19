from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response

from rest_framework.permissions import IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import RoadMapSerializer, TravelCategorySerializer, TravelSerializer, TravelPassengersSerializer, PassengerSerializer
from travels.models import Category, RoadMap, Travel, Category, Passenger


class TravelAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": [
                "list",
                "create",
                "retrieve",
                "update",
                "partial_update",
                "destroy",
            ],
            "principal": [
                "group:admin",
                "group:moder",
            ],
            "effect": "allow"
        }
    ]


class RoadMapView(viewsets.ModelViewSet):
    serializer_class = RoadMapSerializer
    queryset = RoadMap.objects.all()
    permission_classes = [IsAuthenticated, TravelAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]


class TravelCategoryView(viewsets.ModelViewSet):
    serializer_class = TravelCategorySerializer
    queryset = Category.objects.all()
    permission_classes = [IsAuthenticated, TravelAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]


class TravelView(viewsets.ModelViewSet):
    serializer_class = TravelSerializer
    queryset = Travel.objects.all()
    permission_classes = [IsAuthenticated, TravelAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = {
        'arrival__country_from': ['exact'],
        'arrival__country_to': ['exact'],
        'category': ['exact'],
        'date_at': ['exact', 'range'],
        'name': ['exact', 'iexact'],
    }


class TravelPassengersView(viewsets.ModelViewSet):
    serializer_class = TravelPassengersSerializer
    queryset = Travel.objects.all()
    filter_backends = [filters.DjangoFilterBackend]


class PassengersView(viewsets.ModelViewSet):
    serializer_class = PassengerSerializer
    queryset = Passenger.objects.all()
    filter_backends = [filters.DjangoFilterBackend]

    def create(self, request, pk=None):
        try:
            travel = Travel.objects.get(pk=pk)
            serializer = PassengerSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save(travel=travel)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=201, headers=headers)
        except Exception as e:
            return Response({'error': str(e)})
