from rest_framework import serializers

from travels.models import Category, RoadMap, Travel, Passenger


class RoadMapSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoadMap
        fields = (
            'country_from',
            'country_to',
            'uuid'
        )
        read_only_fields = ()


class TravelCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'name',
            'uuid',
        )
        read_only_fields = ()


class PassengerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passenger
        fields = (
            'patient',
            'seat',
            'uuid',
        )


class TravelPassengersSerializer(serializers.ModelSerializer):
    passengers = PassengerSerializer(source='passenger_set', many=True, read_only=True)

    class Meta:
        model = Travel
        fields = (
            'name',
            'category',
            'date_at',
            'data',
            'passengers',
            'uuid',
        )
        read_only_fields = ()


class TravelSerializer(serializers.ModelSerializer):
    arrival = RoadMapSerializer()

    class Meta:
        model = Travel
        fields = (
            'name',
            'category',
            'arrival',
            'date_at',
            'data',
            'uuid',
        )
        read_only_fields = ()
