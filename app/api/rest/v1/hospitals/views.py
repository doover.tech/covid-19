from django_filters import rest_framework as filters
from rest_framework import filters as drf_filters
from rest_framework import viewsets
from rest_framework.response import Response

from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import HospitalSerializer, HospitalCategorySerializer
from hospitals.models import Hospital, HospitalCategory


class HospitalAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": [
                "list",
                "create",
                "retrieve",
                "update",
                "partial_update",
                "destroy",
            ],
            "principal": [
                "group:admin",
                "group:moder",
            ],
            "effect": "allow"
        }
    ]


class HospitalView(viewsets.ModelViewSet):
    serializer_class = HospitalSerializer
    queryset = Hospital.objects.all()
    permission_classes = [IsAuthenticated, HospitalAccessPolicy]
    filter_backends = [drf_filters.SearchFilter, filters.DjangoFilterBackend]
    filterset_fields = ['category', 'region', 'is_heal', 'is_diagnostic']
    search_fields = ['name']

    def get_queryset(self):
        user = self.request.user
        if user.has_perm('hospitals.self_region') and not user.is_superuser:
            return self.queryset.filter(region=user.region)
        return self.queryset


class HospitalCategoryView(viewsets.ModelViewSet):
    serializer_class = HospitalCategorySerializer
    queryset = HospitalCategory.objects.all()
    permission_classes = [IsAuthenticated, HospitalAccessPolicy]
