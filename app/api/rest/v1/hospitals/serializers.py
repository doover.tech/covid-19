from rest_framework import serializers

from hospitals.models import Hospital, HospitalCategory
from locations.models import Region, Address

from api.rest.v1.persons.serializers import PersonSerializer
from api.rest.v1.locations.serializers import AddressSerializer, RegionSerializer


class HospitalCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = HospitalCategory
        fields = (
            'name',
            'uuid',
        )
        read_only_fields = (
            'uuid',
        )


class HospitalSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    class Meta:
        model = Hospital
        fields = (
            'name',
            'is_heal',
            'is_diagnostic',
            'category',
            'address',
            'region',
            'status',
            'uuid',
        )
        read_only_fields = (
            'status',
            'address'
        )

    def get_status(self, obj):
        return {
            'beds_amount': obj.beds_amount,
            'staff_amount': obj.staff_amount,
            'tests_amount': obj.tests_amount,
            'tests_used': obj.tests_used,
        }
