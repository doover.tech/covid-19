from django_filters import rest_framework as filters
from rest_framework import filters as drf_filters
from rest_framework import viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.db.models import Q

from rest_framework.permissions import IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import StateSerializer, PatientSerializer, PatientCreateSerializer, PatientStatusSerializer, PatientStateSerializer
from patients.models import State, Patient, PatientStatus, PatientState


class PatientAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": [
                "list",
                "list_status",
                "create",
                "retrieve",
                "update",
                "partial_update",
                "destroy",
            ],
            "principal": [
                "group:admin",
                "group:moder",
            ],
            "effect": "allow"
        }, {
            "action": [
                "list",
                "retrieve",
            ],
            "principal": [
                "group:check",
            ],
            "effect": "allow"
        }
    ]


class StateView(viewsets.ModelViewSet):
    serializer_class = StateSerializer
    queryset = State.objects.all()
    permission_classes = [IsAuthenticated, PatientAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]


class PatientCreateView(viewsets.ModelViewSet):
    serializer_class = PatientCreateSerializer
    queryset = Patient.objects.all()
    permission_classes = [IsAuthenticated, PatientAccessPolicy]
    filter_backends = [drf_filters.SearchFilter, filters.DjangoFilterBackend]

    def create(self, request):
        try:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save(create_by=request.user)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=201, headers=headers)
        except Exception as e:
            return Response({'error': str(e)})


class PatientView(viewsets.ModelViewSet):
    serializer_class = PatientSerializer
    queryset = Patient.objects.all()
    permission_classes = [IsAuthenticated, PatientAccessPolicy]
    filter_backends = [drf_filters.SearchFilter, filters.DjangoFilterBackend]
    filterset_fields = {
        'person__iin': ['exact'],
        'person__last_name': ['exact', 'iexact'],
        'person__first_name': ['exact', 'iexact'],
        'person__middle_name': ['exact', 'iexact'],
        'region': ['exact'],
        'status__method_infection': ['exact'],
        'status__type_case': ['exact'],
        'status__type_entry': ['exact'],
        'status__type_infection': ['exact'],
        'status__type_hospitalization': ['exact'],
        'status__type_status': ['exact'],
    }
    search_fields = ['person__iin', 'person__last_name', 'person__first_name', 'person__middle_name']

    def get_queryset(self):
        user = self.request.user
        if user.has_perm('patients.self_region') and not user.is_superuser:
            return self.queryset.filter(Q(region=user.region) | Q(create_by=user))
        if user.has_perm('patients.self_patient') and not user.is_superuser:
            return self.queryset.filter(create_by=user)
        return self.queryset


class PatientStatusView(viewsets.ModelViewSet):
    serializer_class = PatientStatusSerializer
    queryset = PatientStatus.objects.all()
    permission_classes = [IsAuthenticated, PatientAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]

    def retrieve(self, request, pk=None):
        status = get_object_or_404(self.queryset, patient__pk=pk)
        serializer = self.serializer_class(status)
        return Response(serializer.data)

    def list_status(self, request):
        def to_dict(choices):
            return dict((k, i) for k, i in choices)
        item = PatientStatus()
        result = {
            'method_infection': to_dict(item.MethodInfectionChoices.choices),
            'type_case': to_dict(item.CaseChoices.choices),
            'type_entry': to_dict(item.EntryChoices.choices),
            'type_infection': to_dict(item.InfectionChoices.choices),
            'type_hospitalization': to_dict(item.HospitalizationChoices.choices),
            'type_status': to_dict(item.StatusChoices.choices),
        }
        return Response(result, status=200)


class PatientStateView(viewsets.ModelViewSet):
    serializer_class = PatientStateSerializer
    queryset = PatientState.objects.all()
    permission_classes = [IsAuthenticated, PatientAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]

    def create(self, request, pk=None):
        try:
            patient = Patient.objects.get(pk=pk)
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save(patient=patient)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=201, headers=headers)
        except Exception as e:
            return Response({'error': str(e)})

    def retrieve(self, request, pk=None, state=None):
        try:
            queryset = self.queryset.filter(patient=pk)
            state = get_object_or_404(queryset, uuid=state)
            serializer = self.serializer_class(state)
            return Response(serializer.data)
        except Exception as e:
            return Response({'error': str(e)})
