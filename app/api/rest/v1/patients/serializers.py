from rest_framework import serializers

from patients.models import State, Patient, PatientStatus, PatientState
from api.rest.v1.persons.serializers import PersonSerializer


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = (
            'name',
            'uuid',
        )
        read_only_fields = ()


class PatientStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientState
        fields = (
            'state',
            'comment',
            'uuid',
        )
        read_only_fields = ()


class PatientStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientStatus
        fields = (
            'method_infection',
            'type_case',
            'type_entry',
            'type_infection',
            'type_hospitalization',
            'type_status',
        )
        read_only_fields = ()


class ContactedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = (
            'person',
            'hospital',
            'uuid',
        )
        read_only_fields = ()


class PatientCreateSerializer(serializers.ModelSerializer):
    status = PatientStatusSerializer()

    class Meta:
        model = Patient
        fields = (
            'person',
            'hospital',
            'status',
            'uuid',
        )
        read_only_fields = ()


class PatientSerializer(serializers.ModelSerializer):
    person = PersonSerializer()
    status = PatientStatusSerializer(read_only=True)
    states = PatientStateSerializer(many=True, read_only=True)
    contacted = ContactedSerializer(many=True, read_only=True)

    class Meta:
        model = Patient
        fields = (
            'person',
            'hospital',
            'status',
            'states',
            'contacted',
            'uuid',
        )
        read_only_fields = ()
