from rest_framework import serializers

from patients.models import Patient
from api.rest.v1.patients.serializers import PatientStatusSerializer


class MapSerializer(serializers.ModelSerializer):
    cordinates = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    state = serializers.SerializerMethodField()
    status = PatientStatusSerializer(read_only=True)

    class Meta:
        model = Patient
        fields = (
            'cordinates',
            'full_name',
            'state',
            'status',
            'uuid',
        )
        read_only_fields = ()

    def get_state(self, obj):
        last_state = obj.states.last()
        if not last_state:
            return None
        return last_state.state.name

    def get_full_name(self, obj):
        person = obj.person
        if not person:
            return None
        return person.full_name

    def get_cordinates(self, obj):
        try:
            if (address := obj.person.extras.address):
                return {
                    'latitude': address.point.x,
                    'longitude': address.point.y,
                }
        except:
            return None
