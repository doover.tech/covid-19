from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response

from rest_framework.permissions import IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import MapSerializer
from patients.models import Patient
import hashlib
import json


class MapAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["list", "hash"],
            "principal": "*",
            "effect": "allow"
        }
    ]


class DashBoardView(viewsets.ViewSet):
    def retrieve(self, request):
        patients = Patient.objects.all()
        counters = {
            'total': patients.count(),
            'alive': patients.filter(status__type_status=1).count(),
            'dead': patients.filter(status__type_status=0).count(),
            'contacted': patients.filter(status__type_status__isnull=True, status__type_infection__isnull=False).count(),
        }
        return Response({'counters': counters}, status=200)


class MapView(viewsets.ModelViewSet):
    serializer_class = MapSerializer
    queryset = Patient.objects.all()
    permission_classes = [IsAuthenticated, MapAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]

    @method_decorator(cache_page(60*60*2))
    def list(self, request):
        queryset = Patient.objects.all()
        serializer = MapSerializer(queryset, many=True)
        return Response(serializer.data)

    @method_decorator(cache_page(60*60*2))
    def hash(self, request):
        map_list = json.dumps(self.list(request).data)
        md5 = hashlib.md5(map_list.encode("utf-8"))
        return Response({'hash': md5.hexdigest()})
