from rest_framework import serializers

from persons.models import Person, PersonContact, PersonExtra


class PersonContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonContact
        fields = (
            'kind',
            'value',
            'uuid',
        )
        read_only_fields = ()


class PersonExtraSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonExtra
        fields = (
            'address',
            'job',
            'citizenship',
        )
        read_only_fields = ()


class PersonSerializer(serializers.ModelSerializer):
    contacts = PersonContactSerializer(many=True, read_only=True)
    extras = PersonExtraSerializer(read_only=True)

    class Meta:
        model = Person
        fields = (
            'iin',
            'passport',
            'last_name',
            'first_name',
            'middle_name',
            'gender',
            'birth_date',
            'extras',
            'contacts',
            'uuid',
        )
        read_only_fields = (
            'extras',
            'contacts',
        )
