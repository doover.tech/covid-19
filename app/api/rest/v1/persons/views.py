from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from rest_framework.permissions import IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import PersonSerializer, PersonContactSerializer, PersonExtraSerializer
from persons.models import Person, PersonContact, PersonExtra


class PersonAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": [
                "me",
                "list",
                "create",
                "retrieve",
                "update",
                "partial_update",
                "destroy",
            ],
            "principal": [
                "group:admin",
                "group:moder",
            ],
            "effect": "allow"
        }
    ]


class PersonView(viewsets.ModelViewSet):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()
    permission_classes = [IsAuthenticated, PersonAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = ['gender']


class PersonExtraView(viewsets.ModelViewSet):
    serializer_class = PersonExtraSerializer
    queryset = PersonExtra.objects.all()
    permission_classes = [IsAuthenticated, PersonAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]

    def retrieve(self, request, pk=None):
        queryset = self.queryset
        try:
            person = Person.objects.get(pk=pk)
        except:
            queryset = queryset.none()
        else:
            person_extra, _ = PersonExtra.objects.get_or_create(person=person)
        extra = get_object_or_404(queryset, person__pk=pk)
        serializer = self.serializer_class(extra)
        return Response(serializer.data)


class PersonContactView(viewsets.ModelViewSet):
    serializer_class = PersonContactSerializer
    queryset = PersonContact.objects.all()
    permission_classes = [IsAuthenticated, PersonAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]

    def list(self, request, pk=None):
        queryset = self.queryset.filter(person=pk)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, pk=None):
        try:
            person = Person.objects.get(pk=pk)
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save(person=person)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=201, headers=headers)
        except Exception as e:
            return Response({'error': str(e)})

    def retrieve(self, request, pk=None, value=None):
        try:
            queryset = self.queryset.filter(person=pk)
            contact = get_object_or_404(queryset, value=value)
            serializer = self.serializer_class(contact)
            return Response(serializer.data)
        except Exception as e:
            return Response({'error': str(e)})
