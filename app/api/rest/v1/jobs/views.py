from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response

from rest_framework.permissions import IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import JobSerializer, JobEmploymentSerializer, JobCategorySerializer
from jobs.models import Job, JobEmployment, JobCategory


class JobAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": [
                "list",
                "create",
                "retrieve",
                "update",
                "partial_update",
                "destroy",
            ],
            "principal": [
                "group:admin",
                "group:moder",
            ],
            "effect": "allow"
        }
    ]


class JobEmploymentView(viewsets.ModelViewSet):
    serializer_class = JobEmploymentSerializer
    queryset = JobEmployment.objects.all()
    permission_classes = [IsAuthenticated, JobAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]


class JobCategoryView(viewsets.ModelViewSet):
    serializer_class = JobCategorySerializer
    queryset = JobCategory.objects.all()
    permission_classes = [IsAuthenticated, JobAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]


class JobView(viewsets.ModelViewSet):
    serializer_class = JobSerializer
    queryset = Job.objects.all()
    permission_classes = [IsAuthenticated, JobAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = {
        'category': ['exact'],
        'employment': ['exact',],
    }
