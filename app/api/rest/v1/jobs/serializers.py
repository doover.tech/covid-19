from rest_framework import serializers

from jobs.models import Job, JobCategory, JobEmployment


class JobEmploymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobEmployment
        fields = (
            'name',
            'uuid',
        )
        read_only_fields = (
            'uuid',
        )


class JobCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = JobCategory
        fields = (
            'name',
            'uuid',
        )
        read_only_fields = (
            'uuid',
        )


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = (
            'name',
            'position',
            'address',
            'category',
            'employment',
            'uuid',
        )
        read_only_fields = (
            'uuid',
        )
