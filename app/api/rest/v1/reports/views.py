from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response

from rest_framework.permissions import IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import ReportSerializer, PatientsStatisticsDateRange
from utils.reports import handle_uploaded_patients
from rest_framework import status

from locations.models import Region
from hospitals.models import Hospital
from persons.models import Person, PersonExtra, PersonContact
from patients.models import PatientStatus
from django.http import HttpResponse
from dateutil.relativedelta import relativedelta
import datetime
import csv


class ReportAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": [
                "patients_download",
                "patients_uplaod",
                "patients_download_region_age",
                "patients_statistics_in_date_range",
                "hospitals_download"
            ],
            "principal": [
                "group:admin",
                "group:moder",
            ],
            "effect": "allow"
        }
    ]


class ReportViewSet(viewsets.ModelViewSet):
    serializer_class = ReportSerializer
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        serializer_class = self.serializer_class

        if self.action == 'patients_statistics_in_date_range':
            serializer_class = PatientsStatisticsDateRange
        return serializer_class

    def patients_uplaod(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        handle_uploaded_patients(request.data['csv_file'])
        return Response(status=status.HTTP_201_CREATED, data={'success':'True'})

    def patients_download(self, request):
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="persons.csv"'
        response.write("\ufeff".encode("utf8"))
        writer = csv.writer(response)
        writer.writerow(
            [
                "Фамилия",
                "Имя",
                "Отчество",
                "ИИН",
                "Паспорт",
                "Пол",
                "День рождения",
                "Эмейл",
                "Телефон",
                "Адрес",
                "Дом",
                "Квартира",
                "Здание",
                "Страна",
                "Регион",
                "Город",
                "Страна компании",
                "Регион компании",
                "Город компании",
                "Название компании",
                "Адрес компании",
                "Дом компании",
                "Квартира компании",
                "Здание компании",
                "Больница",
                "Статус инфекция",
                "Тип случая",
                "Тип въезда",
                "Статус госпитализации",
                "Текущее состяние",
                "Способ заражения"
            ]
        )
        for person in Person.objects.filter(patient__isnull=False):

            if PersonContact.objects.filter(person=person, kind=1):
                email = PersonContact.objects.filter(person=person, kind=1).first()
            else:
                email = None

            if PersonContact.objects.filter(person=person, kind=0):
                phone = PersonContact.objects.filter(person=person, kind=0).first()
            else:
                phone = None

            try:
                person_extra = PersonExtra.objects.get(person=person)
                job = person_extra.job
                if job.address:
                    job_address = job.address
                else:
                    job_address = None
            except:
                person_extra = None
                job = None
                job_address = None

            patient = person.patient

            try:
                patient_status = PatientStatus.objects.get(patient=patient)
            except:
                patient_status = None

            writer.writerow(
                [
                    person.first_name,
                    person.last_name,
                    person.middle_name,
                    person.iin,
                    person.passport,
                    person.gender,
                    person.birth_date,
                    f'{email.value if email and email.value else None}',
                    f'{phone.value if phone and phone.value else None}',
                    f'{person_extra.address.name if (person_extra and person_extra.address) else None}',
                    f'{person_extra.address.house if (person_extra and person_extra.address) else None}',
                    f'{person_extra.address.flat if (person_extra and person_extra.address) else None}',
                    f'{person_extra.address.building if (person_extra and person_extra.address) else None}',
                    f'{person_extra.address.city.country.name if (person_extra and person_extra.address and person_extra.address.city.country) else None}',
                    f'{person_extra.address.city.region.name if (person_extra and person_extra.address and person_extra.address.city.region) else None}',
                    f'{person_extra.address.city.name if (person_extra and person_extra.address) else None}',
                    f'{job_address.city.country.name if job_address and job_address.city.country else None}',
                    f'{job_address.city.region.name if job_address and job_address.city.region else None}',
                    f'{job_address.city.name if job_address else None}',
                    f'{job.name if job else None}',
                    f'{job_address.name if job_address else None}',
                    f'{job_address.house if job_address else None}',
                    f'{job_address.flat if job_address else None}',
                    f'{job_address.building if job_address else None}',
                    f'{patient.hospital.name if patient.hospital else None}',
                    f'{patient_status.get_type_infection_display() if patient_status else None}',
                    f'{patient_status.get_type_case_display() if patient_status else None}',
                    f'{patient_status.get_type_hospitalization_display() if patient_status else None}',
                    f'{patient_status.get_type_status_display() if patient_status else None}',
                    f'{patient_status.get_method_infection_display() if patient_status else None}',
                ]
            )
        return response

    def patients_download_region_age(self, request):
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="patients_regions.csv"'
        response.write("\ufeff".encode("utf8"))
        now = datetime.datetime.now().date()
        writer = csv.writer(response)
        writer.writerow(
            [
                'Регион',
                'Все',
                'М 0-9',
                'Ж 0-9',
                'М 10-19',
                'Ж 10-19',
                'М 20-29',
                'Ж 20-29',
                'М 30-39',
                'Ж 30-39',
                'М 40-49',
                'Ж 40-49',
                'М 50-59',
                'Ж 50-59',
                'М 60-69',
                'Ж 60-69',
                'М 70-79',
                'Ж 70-79',
                'М 80-89',
                'Ж 80-89',
                'М 90-99',
                'Ж 90-99',

            ]
        )
        for region in Region.objects.all():
            writer.writerow(
                [
                    region.name,
                    region.patients.all().count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=10), now]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=10), now]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=20),
                                                                      now - relativedelta(years=11)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=20),
                                                                      now - relativedelta(years=11)]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=30),
                                                                      now - relativedelta(years=21)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=30),
                                                                      now - relativedelta(years=21)]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=40),
                                                                      now - relativedelta(years=31)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=40),
                                                                      now - relativedelta(years=31)]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=50),
                                                                      now - relativedelta(years=41)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=50),
                                                                      now - relativedelta(years=41)]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=60),
                                                                      now - relativedelta(years=51)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=60),
                                                                      now - relativedelta(years=51)]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=70),
                                                                      now - relativedelta(years=61)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=70),
                                                                      now - relativedelta(years=61)]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=80),
                                                                      now - relativedelta(years=71)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=80),
                                                                      now - relativedelta(years=71)]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=90),
                                                                      now - relativedelta(years=81)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=90),
                                                                      now - relativedelta(years=81)]).count(),
                    region.patients.filter(person__gender=1,
                                           person__birth_date__range=[now - relativedelta(years=100),
                                                                      now - relativedelta(years=91)]).count(),
                    region.patients.filter(person__gender=0,
                                           person__birth_date__range=[now - relativedelta(years=100),
                                                                      now - relativedelta(years=91)]).count(),
                ]
            )
        return response

    def patients_statistics_in_date_range(self, request):
        serializer_calss = self.get_serializer_class()
        serializer = serializer_calss(data=request.data)
        serializer.is_valid(raise_exception=True)

        first_date = serializer.validated_data['first_date']
        second_date = serializer.validated_data['second_date']
        third_date = first_date - (second_date - first_date)

        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="patients_statistics.csv"'
        response.write("\ufeff".encode("utf8"))
        writer = csv.writer(response)

        writer.writerow(
            [
                'Регион',
                f'Инфицировано за {first_date} - {second_date}',
                'Прирост (%)',
                'Завозные',
                '%',
                'Контактные',
                '%'
            ]
        )

        for region in Region.objects.all():
            new_patients = region.patients.filter(created_at__range=[first_date, second_date]).count(),
            old_patients = region.patients.filter(created_at__range=[third_date, first_date]).count(),

            try:
                patients_percent_plus = (new_patients[0] * 100) / old_patients[0]
            except ZeroDivisionError:
                patients_percent_plus = 0

            new_imported = region.patients.filter(created_at__range=[first_date, second_date],
                                                  status__type_case=1).count(),
            old_imported = region.patients.filter(created_at__range=[third_date, first_date],
                                                  status__type_case=1).count(),
            try:
                imported_percent_plus = (new_imported[0] * 100) / old_imported[0]
            except ZeroDivisionError:
                imported_percent_plus = 0


            new_contacted = region.patients.filter(created_at__range=[first_date, second_date],
                                                  status__type_case=2).count(),
            old_contacted = region.patients.filter(created_at__range=[third_date, first_date],
                                                  status__type_case=2).count(),
            try:
                contacted_percent_plus = (new_contacted[0] * 100) / old_contacted[0]
            except ZeroDivisionError:
                contacted_percent_plus = 0

            writer.writerow(
                [
                    region.name,
                    new_patients,
                    patients_percent_plus,
                    new_imported,
                    imported_percent_plus,
                    new_contacted,
                    contacted_percent_plus
                ]
            )

        return response

    def hospitals_download(self, request):
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="hospitals.csv"'
        response.write("\ufeff".encode("utf8"))
        writer = csv.writer(response)
        writer.writerow(
            [
                'Название',
                'Категория',
                'Регион',
                'Адрес',
                'Дом',
                'Квартира',
                'Здание',
                'Страна',
                'Город',
                'Лечебная',
                'Диагностическая',
                'Количество коек',
                'Количество сотрудников',
                'Количество тество',
                'Количество использованных тестов'
            ]
        )

        for hospital in Hospital.objects.all():
            writer.writerow(
                [
                    hospital.name,
                    hospital.category,
                    hospital.region,
                    f'{hospital.address.name if hospital.address else None}',
                    f'{hospital.address.house if hospital.address else None}',
                    f'{hospital.address.flat if hospital.address else None}',
                    f'{hospital.address.building if hospital.address else None}',
                    f'{hospital.address.city.country if hospital.address and hospital.address.city.country else None}',
                    f'{hospital.address.city if hospital.address and hospital.address.city else None}',
                    hospital.is_heal,
                    hospital.is_diagnostic,
                    hospital.beds_amount,
                    hospital.staff_amount,
                    hospital.tests_amount,
                    hospital.tests_used
                ]
            )

        return response