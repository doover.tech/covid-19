from rest_framework import serializers


class ReportSerializer(serializers.Serializer):
    csv_file = serializers.FileField()


class PatientsStatisticsDateRange(serializers.Serializer):
    first_date = serializers.DateTimeField()
    second_date = serializers.DateTimeField()