from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import CountrySerializer, RegionSerializer, CitySerializer, RegionPatientsSerializer
from .serializers import AddressSerializer
from locations.models import Country, Region, City
from locations.models import Address


class LocationAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": [
                "list",
                "create",
                "retrieve",
                "update",
                "partial_update",
                "destroy",
                "patients",
            ],
            "principal": [
                "group:admin",
                "group:moder",
            ],
            "effect": "allow"
        }
    ]


class CountryView(viewsets.ModelViewSet):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()
    permission_classes = [IsAuthenticated, LocationAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]


class RegionView(viewsets.ModelViewSet):
    serializer_class = RegionSerializer
    queryset = Region.objects.all()
    permission_classes = [IsAuthenticated, LocationAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = ['country']

    def patients(self, request):
        serializer = RegionPatientsSerializer(self.queryset, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class CityView(viewsets.ModelViewSet):
    serializer_class = CitySerializer
    queryset = City.objects.all()
    permission_classes = [IsAuthenticated, LocationAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = ['country', 'region']


class AddressView(viewsets.ModelViewSet):
    serializer_class = AddressSerializer
    queryset = Address.objects.all()
    permission_classes = [IsAuthenticated, LocationAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]
