from rest_framework import serializers

from locations.models import Country, Region, City
from locations.models import Address, AddressCategory
from django.db.models import Q


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = (
            'name',
            'iso',
            'mok',
            'uuid',
        )
        read_only_fields = ()


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = (
            'name',
            'country',
            'uuid',
        )
        read_only_fields = ()


class RegionPatientsSerializer(serializers.ModelSerializer):
    confirm = serializers.SerializerMethodField()
    recover = serializers.SerializerMethodField()
    death = serializers.SerializerMethodField()
    contacted = serializers.SerializerMethodField()

    class Meta:
        model = Region
        fields = (
            'name',
            'confirm',
            'recover',
            'death',
            'contacted',
        )
        read_only_fields = ()

    def get_confirm(self, obj):
        return obj.patients.filter(status__isnull=False, status__type_status__isnull=True).count()

    def get_recover(self, obj):
        return obj.patients.filter(status__type_status=1).count()

    def get_death(self, obj):
        return obj.patients.filter(status__type_status=0).count()

    def get_contacted(self, obj):
        return obj.patients.filter(status__type_status__isnull=True, status__type_infection__isnull=False).count()


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = (
            'name',
            'country',
            'region',
            'uuid',
        )
        read_only_fields = ()


class AddressSerializer(serializers.ModelSerializer):
    cordinates = serializers.SerializerMethodField()

    class Meta:
        model = Address
        fields = (
            'name',
            'city',
            'street',
            'house',
            'flat',
            'building',
            'cordinates',
            'uuid',
        )
        read_only_fields = (
            'name',
        )

    def get_cordinates(self, obj):
        if not obj.point:
            return None
        return {
            'latitude': obj.point.x,
            'longitude': obj.point.y,
        }
