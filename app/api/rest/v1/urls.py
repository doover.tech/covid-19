from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from .hospitals.views import HospitalView, HospitalCategoryView
from .locations.views import CountryView, RegionView, CityView, AddressView
from .persons.views import PersonView, PersonExtraView, PersonContactView
from .patients.views import PatientView, PatientCreateView, PatientStatusView, StateView, PatientStateView
from .reports.views import ReportViewSet
from .travels.views import RoadMapView, TravelCategoryView, TravelView, TravelPassengersView, PassengersView
from .jobs.views import JobView, JobEmploymentView, JobCategoryView
from .dashboard.views import DashBoardView, MapView
from .users.views import UserView
# Swager
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

app_name = 'rest_v1'

schema_view = get_schema_view(
   openapi.Info(
      title="Covid-19",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

swager_patterns = [
    path('', schema_view.without_ui(cache_timeout=0), name='api.docs.json'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='api.docs.redoc'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='api.docs.swagger'),
]

urlpatterns = [
    # Me routes
    path('me/', UserView.as_view({'get': 'me'}), name='users.me'),
    # Hospitals routes
    path('hospitals/', HospitalView.as_view({'get': 'list'}), name='hospitals.list'),
    path('hospitals/create/', HospitalView.as_view({'post': 'create'}), name='hospitals.create'),
    path('hospitals/categories/', HospitalCategoryView.as_view({'get': 'list'}), name='hospitals.categories'),
    path('hospitals/<uuid:pk>/', HospitalView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='hospitals.detail'),
    # Locations routes
    path('countries/', CountryView.as_view({'get': 'list'}), name='countries.list'),
    path('countries/create/', CountryView.as_view({'post': 'create'}), name='countries.create'),
    path('countries/<uuid:pk>/', CountryView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='countries.detail'),
    # Locations routes
    path('regions/', RegionView.as_view({'get': 'list'}), name='regions.list'),
    path('regions/create/', RegionView.as_view({'post': 'create'}), name='regions.create'),
    path('regions/<uuid:pk>/', RegionView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='regions.detail'),
    path('regions/patients/', RegionView.as_view({'get':'patients'})),
    # Locations routes
    path('cities/', CityView.as_view({'get': 'list'}), name='cities.list'),
    path('cities/create/', CityView.as_view({'post': 'create'}), name='cities.create'),
    path('cities/<uuid:pk>/', CityView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='cities.detail'),
    # Locations routes
    path('addresses/', AddressView.as_view({'get': 'list'}), name='addresses.list'),
    path('addresses/create/', AddressView.as_view({'post': 'create'}), name='addresses.create'),
    path('addresses/<uuid:pk>/', AddressView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='addresses.detail'),
    # Persons routes
    path('persons/', PersonView.as_view({'get': 'list'}), name='persons.list'),
    path('persons/create/', PersonView.as_view({'post': 'create'}), name='persons.create'),
    path('persons/<uuid:pk>/', PersonView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='persons.detail'),
    path('persons/<uuid:pk>/extra/', PersonExtraView.as_view({'get': 'retrieve', 'put': 'update'}), name='persons.extra.detail'),
    path('persons/<uuid:pk>/contacts/', PersonContactView.as_view({'get': 'list', 'post': 'create'}), name='persons.contact.list'),
    path('persons/<uuid:pk>/contacts/<str:value>/', PersonContactView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='persons.contact.detail'),
    # States routes
    path('states/', StateView.as_view({'get': 'list'}), name='states.list'),
    path('states/create/', StateView.as_view({'post': 'create'}), name='states.create'),
    path('states/<uuid:pk>/', StateView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='states.detail'),
    # Patients routes
    path('patients/', PatientView.as_view({'get': 'list'}), name='patients.list'),
    path('patients/create/', PatientCreateView.as_view({'post': 'create'}), name='patients.create'),
    path('patients/status/', PatientStatusView.as_view({'get': 'list_status'}), name='patients.status'),
    path('patients/<uuid:pk>/', PatientView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='patients.detail'),
    path('patients/<uuid:pk>/status/', PatientStatusView.as_view({'get': 'retrieve', 'put': 'update'}), name='patients.status.detail'),
    path('patients/<uuid:pk>/states/', PatientStateView.as_view({'get': 'list', 'post': 'create'}), name='patients.state.list'),
    path('patients/<uuid:pk>/states/<uuid:state>/', PatientStateView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='patients.state.detail'),
    # Users routes
    path('users/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('users/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    # Reports routes
    path('reports/patients/upload/', ReportViewSet.as_view({'post':'patients_uplaod'})),
    path('reports/patients/download/', ReportViewSet.as_view({'get':'patients_download'})),
    path('reports/patients/regions/download/', ReportViewSet.as_view({'get':'patients_download_region_age'})),
    path('reports/patients/date/download/', ReportViewSet.as_view({'post':'patients_statistics_in_date_range'})),
    path('reports/hospitals/download/', ReportViewSet.as_view({'get':'hospitals_download'})),
    # Travels routes
    path('roadmaps/', RoadMapView.as_view({'get': 'list'}), name='roadmap.list'),
    path('roadmaps/create/', RoadMapView.as_view({'post': 'create'}), name='roadmap.create'),
    path('roadmaps/<uuid:pk>/', RoadMapView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='roadmap.detail'),
    # Travels routes
    path('travels/', TravelView.as_view({'get': 'list'}), name='travels.list'),
    path('travels/create/', TravelView.as_view({'post': 'create'}), name='travels.create'),
    path('travels/categories/', TravelCategoryView.as_view({'get': 'list'}), name='travels.categories'),
    path('travels/<uuid:pk>/', TravelView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='travels.detail'),
    path('travels/<uuid:pk>/passengers/', TravelPassengersView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='travels.passgenders'),
    path('travels/<uuid:pk>/passengers/create/', PassengersView.as_view({'post': 'create'}), name='travels.passgenders.create'),
    # Jobs routes
    path('jobs/', JobView.as_view({'get': 'list'}), name='jobs.list'),
    path('jobs/create/', JobView.as_view({'post': 'create'}), name='jobs.create'),
    path('jobs/categories/', JobCategoryView.as_view({'get': 'list'}), name='jobs.categories'),
    path('jobs/employments/', JobEmploymentView.as_view({'get': 'list'}), name='jobs.employments'),
    path('jobs/<uuid:pk>/', JobView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='jobs.detail'),
    # Other routes
    path('dashboard/', DashBoardView.as_view({'get': 'retrieve'}), name='dashboard.detail'),
    path('maps/', MapView.as_view({'get': 'list'}), name='maps.list'),
    path('maps/hash/', MapView.as_view({'get': 'hash'}), name='maps.hash'),
]

urlpatterns += swager_patterns
