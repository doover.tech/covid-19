from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from rest_framework.permissions import IsAuthenticated
from rest_access_policy import AccessPolicy

from .serializers import UserSerializers
from users.models import CustomUser


class UserAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": [
                "me",
                "list",
                "create",
                "retrieve",
                "update",
                "partial_update",
                "destroy",
            ],
            "principal": [
                "group:admin",
                "group:moder",
            ],
            "effect": "allow"
        }
    ]


class UserView(viewsets.ModelViewSet):
    serializer_class = UserSerializers
    queryset = CustomUser.objects.all()
    permission_classes = [IsAuthenticated, UserAccessPolicy]
    filter_backends = [filters.DjangoFilterBackend]

    def me(self, request):
        serializer = self.serializer_class(request.user)
        return Response(serializer.data)
