from rest_framework import serializers

from api.rest.v1.persons.serializers import PersonSerializer
from users.models import CustomUser


class UserSerializers(serializers.ModelSerializer):
    person = PersonSerializer()
    groups = serializers.SerializerMethodField()

    class Meta:
        model = CustomUser
        fields = (
            'iin',
            'person',
            'groups',
            'user_permissions',
            'uuid',
        )

    def get_groups(self, obj):
        return [group.name for group in obj.groups.all()]
