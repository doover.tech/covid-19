from django.contrib.auth import get_user_model
import graphene

from graphene_django.types import DjangoObjectType


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        exclude = (
            'password',
            'is_superuser',
        )


class Query:
    me = graphene.Field(UserType)

    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')

        return user
