import graphene

from graphene_django.types import DjangoObjectType
from locations.models import Country, City, Region, Address, AddressCategory


class CountryType(DjangoObjectType):
    class Meta:
        model = Country


class CityType(DjangoObjectType):
    class Meta:
        model = City


class RegionType(DjangoObjectType):
    class Meta:
        model = Region


class AddressType(DjangoObjectType):
    class Meta:
        model = Address
        exclude = ('point',)


class AddressCategoryType(DjangoObjectType):
    class Meta:
        model = AddressCategory


class Query:
    pass
