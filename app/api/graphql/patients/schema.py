from django.db.models import Q
import graphene

from graphene_django.types import DjangoObjectType
from patients.models import Patient, PatientStatus, State, PatientState


class StateType(DjangoObjectType):
    class Meta:
        model = State


class PatientType(DjangoObjectType):
    class Meta:
        model = Patient


class PatientStatusType(DjangoObjectType):
    class Meta:
        model = PatientStatus
        convert_choices_to_enum = False


class PatientStateType(DjangoObjectType):
    class Meta:
        model = PatientState


class Query:
    patient = graphene.Field(
        PatientType,
        iin=graphene.String(),
        passport=graphene.String()
    )

    def resolve_patient(self, info, iin=None, passport=None, **kwargs):
        queryset = Patient.objects.all()

        if iin:
            queryset = queryset.filter(person__iin=iin).first()
        if passport:
            queryset = queryset.filter(person__passport=passport).first()

        return queryset
