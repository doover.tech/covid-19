import graphene

from graphene_django.types import DjangoObjectType
from jobs.models import Job, JobCategory


class JobType(DjangoObjectType):
    class Meta:
        model = Job


class JobCategoryType(DjangoObjectType):
    class Meta:
        model = JobCategory


class Query:
    pass
