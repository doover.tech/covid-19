import graphene

from graphene_django.types import DjangoObjectType
from travels.models import RoadMap, Travel


class RoadMapType(DjangoObjectType):
    class Meta:
        model = RoadMap


class TravelType(DjangoObjectType):
    class Meta:
        model = Travel


class Query:
    pass
