import graphene

from graphene_django.types import DjangoObjectType
from persons.models import Person, PersonContact, PersonExtra


class PersonContactType(DjangoObjectType):
    class Meta:
        model = PersonContact


class PersonExtraType(DjangoObjectType):
    class Meta:
        model = PersonExtra


class PersonType(DjangoObjectType):
    class Meta:
        model = Person
        convert_choices_to_enum = False


class Query:
    all_persons = graphene.List(PersonType)

    def resolve_all_persons(self, info, **kwargs):
        return Person.objects.all()
