import graphene

from graphene_django.types import DjangoObjectType
from hospitals.models import Hospital, HospitalCategory


class HospitalType(DjangoObjectType):
    class Meta:
        model = Hospital


class HospitalCategoryType(DjangoObjectType):
    class Meta:
        model = HospitalCategory


class Query:
    pass
