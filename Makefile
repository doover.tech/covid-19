env:
	poetry shell
clean:
	find app/ -path "*/migrations/*.py" -not -name "__init__.py" -delete
	find app/ -path "*/migrations/*.pyc"  -delete
	find app/ -path "*/run/dev.sqlite3"  -delete
run:
	python app/manage.py makemigrations
	python app/manage.py migrate
	python app/manage.py runserver 0.0.0.0:9000
run_plus:
	python app/manage.py makemigrations
	python app/manage.py migrate
	python app/manage.py runserver_plus 0.0.0.0:9000
createsuperuser:
	python app/manage.py createsuperuser
jupiter:
	python app/manage.py shell_plus --notebook
requirements:
	poetry export --without-hashes -f requirements.txt > requirements.txt
	poetry export --dev --without-hashes -f requirements.txt > requirements-dev.txt
